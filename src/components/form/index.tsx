import * as React from 'react';
import './skeleton.css';
import './main.css';
import { 
    Formik, 
    Field, 
    Form, 
    FormikActions
} from 'formik';
import Axios from 'axios';
import { Request } from '../../services/request';

interface Values {
  firstName: string;
  lastName: string;
  email: string;
  message: string;
}

const MainForm: React.SFC<{}> = () => (
  <div className="container">
    <h1>Help Support</h1>
    <Formik
      initialValues={{
        firstName: '',
        lastName: '',
        email: '',
        message: ''
      }}
      onSubmit={async (values: Values, { setSubmitting, setStatus, resetForm}: FormikActions<Values>) => {
          try {
            resetForm()
            const response = await Axios.post(Request, values);
            alert(response.data);
            setSubmitting(false);
            setStatus({success: true})
          } catch (error) {
            setStatus({success: false})
            setSubmitting(false)
          }
      }}
      render={() => (
        <Form>
          <label htmlFor="firstName">First Name</label>
          <Field id="firstName" name="firstName" placeholder="your first name" type="text" />

          <label htmlFor="lastName">Last Name</label>
          <Field id="lastName" name="lastName" placeholder="your last name" type="text" />

          <label htmlFor="email">Email</label>
          <Field id="email" name="email" placeholder="your email" type="email" />

          <label htmlFor="email">Message</label>
          <Field component="textarea" id="message" name="message" placeholder="how can we help?" type="text" />

          <button type="submit" style={{ display: 'block' }}>
            Submit
          </button>
        </Form>
      )}
    />
  </div>
);


export default MainForm;
import * as React from 'react';

const styles = {
  textStyles: {
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
  }
}

export const Footer: React.StatelessComponent<{}> = () => {
  const { textStyles } = styles;

  return (
   <>
        <span style={textStyles}>
            Copyright © Gowtham
        </span>
   </>   
  )
}
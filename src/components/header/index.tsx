import * as React from 'react';
import { withStyles, WithStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const styles = {
  root: {
    flexGrow: 1,
    marginBottom: 50
  }
};


export interface Props extends WithStyles<typeof styles> {
    classes: any;
}

const HeaderComponentInner = (props: Props) => {
const {classes} = props;
    return (
      <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h3" color="inherit" className={classes.grow}>
          VRCOLLAB
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
    );
}

export const Header = withStyles(styles)(
    HeaderComponentInner
)



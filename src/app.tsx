import * as React from 'react';
import { Header } from './components/header';
import { Footer } from "./components/footer";
import MainForm from './components/form';

export const App = () => {
    return (
        <>
            <div className="header">
                <Header />
            </div>
            <div className="main">
                <MainForm />
            </div>
            <div className= "footer" style={{ paddingTop: 60 }}>
                <Footer />
            </div>
        </>
    )
}
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { App } from "./app";
import { setConfig } from 'react-hot-loader'

setConfig({
  ignoreSFC: true, // RHL will be __completely__ disabled for SFC
  pureRender: true, // RHL will not change render method
})

 
ReactDOM.render(<App />, document.getElementById("root"));
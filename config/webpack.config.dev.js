const webpack = require('webpack'),   
      HtmlWebpackPlugin = require('html-webpack-plugin'),         
      MiniCssExtractPlugin = require("mini-css-extract-plugin"),
      helper = require('./helper'),
      path = require('path')


module.exports = {
  devtool: "source-map",
  mode: 'development',
  entry: helper.root('src','index'),
  target: "web",
  resolve: {
    extensions: ['.js', '.json', '.ts', '.tsx']
  },
  module: {
    rules: [
      { test: /\.ts?$/, use: "ts-loader", exclude: /node_modules/  },
      { test: /\.tsx?$/, use: "ts-loader", exclude: /node_modules/  },
      { test: /\.js?$/, use: "babel-loader", exclude: /node_modules/ },
      {
        test: /\.scss$/,
        include: [
          helper.root('src/components', 'form')
        ],
        use: [
          MiniCssExtractPlugin.loader, 
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              modules: true
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.css/,
        use: [MiniCssExtractPlugin.loader, 'css-loader']
      },
      {
        test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)$/,
        use: 'file-loader'
      },
      {
        test: /\.(html)$/,
        use: {
          loader: 'html-loader',
          options: {
            attrs: [':data-src']
          }
        }
      }
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "styles.css",
      chunkFilename: "styles.css"
    }),
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new HtmlWebpackPlugin({
      template: helper.root('src','index.html')
    }),
    new webpack.DefinePlugin({
      "process.env": { 
         NODE_ENV: JSON.stringify("development") 
       }
    })
  ],
  externals: {
    jquery: 'jQuery'
  },
   devServer: {
      inline: true,
      contentBase: helper.root('dist'),
      historyApiFallback: true,
      port: 1337,
      disableHostCheck: true
    },
    output: {
      path: helper.root('dist'),
      filename: 'bundle.js'
    },
};
